resource "aws_security_group" "this" {
  name   = "${var.name}-db-sg"
  vpc_id = data.aws_vpc.selected.id

  egress {
    cidr_blocks = [data.aws_vpc.selected.cidr_block]
    description = "allow only to vpc"
    from_port   = var.database_port
    to_port     = var.database_port
    protocol    = "TCP"
  }

  lifecycle {
    create_before_destroy = true
  }

  tags = {
    Name = "${var.name}-db-sg"
  }
}

resource "aws_security_group_rule" "this" {
  cidr_blocks       = data.aws_subnet.private.*.cidr_block
  description       = "postgres-port"
  from_port         = var.database_port
  to_port           = var.database_port
  type              = "ingress"
  protocol          = "tcp"
  security_group_id = aws_security_group.this.id
}

resource "aws_security_group_rule" "bastion" {
  count             = var.bastion_cidr != "" ? 1 : 0
  cidr_blocks       = [var.bastion_cidr]
  description       = "bastion-postgres-access"
  from_port         = var.database_port
  to_port           = var.database_port
  type              = "ingress"
  protocol          = "tcp"
  security_group_id = aws_security_group.this.id
}
