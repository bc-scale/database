resource "aws_db_subnet_group" "this" {
    name       = "${var.name}-db-subnet-group"
    subnet_ids = tolist(data.aws_subnet_ids.private.ids)
    tags = {
      Name = "${var.name}-db-subnet-group"
    }
}

module "kms_key" {
    source   = "git@gitlab.com:terraform147/kms-key.git"
    app_name = "${var.name}-db"
}

resource "aws_db_instance" "default" {
    allocated_storage            = var.db_size
    ca_cert_identifier           = "rds-ca-2019"
    backup_retention_period      = var.backup_retention_days
    backup_window                = "03:00-04:00"
    db_subnet_group_name         = aws_db_subnet_group.this.name
    deletion_protection          = true
    engine                       = var.database_engine
    engine_version               = var.database_engine_version
    final_snapshot_identifier    = "${var.name}-db-final-snapshot"
    identifier                   = "${var.name}-db"
    instance_class               = var.instance_type
    maintenance_window           = "Sun:04:01-Sun:05:00"
    max_allocated_storage        = var.db_size + 300
    multi_az                     = true
    name                         = local.name_under
    password                     = random_password.db_master_pass.result
    performance_insights_enabled = true
    skip_final_snapshot          = false
    storage_encrypted            = var.encrypted
    kms_key_id                   = module.kms_key.arn
    storage_type                 = "gp2"
    username                     = var.database_user
    vpc_security_group_ids       = [aws_security_group.this.id]
    tags = {
        Product = var.name
    }
}
